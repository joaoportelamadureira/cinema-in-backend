const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./datas/database.sqlite"
});

async function testConnectionDatabase() {
    try {
        await sequelize.authenticate();
    } catch {
        console.log("Não foi possível conectar ao banco de dados")
    }
}

testConnectionDatabase();

module.exports = sequelize;