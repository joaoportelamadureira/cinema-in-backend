const sequelize = require("../config/database");

async function createDatabase() {
    try {
        await sequelize.sync({ alter: true });
        console.log("Banco de dados criado com sucesso!")
    } catch {
        console.log("Erro na criação do banco de dados!")
    }
}

module.exports = createDatabase;