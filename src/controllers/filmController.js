const Film = require('../models/film.models');

async function createFilm(request, response) {
    try {
        
        const { title, url, sinopsys, genre, classification, director } = request.body;

        console.log(title, url, sinopsys, genre, classification, director)
        
        const film = await Film.create({
            title,
            url,
            sinopsys,
            genre,
            classification,
            director
        });

        return response.status(201).json(film);
        
    } catch (error) {
        return response.status(500).json({ error: "Ocorreu um erro ao tentar registrar o filme." });
        }
}

async function getAllFilms(req, res) {
    try {
        const films = await Film.findAll();
            
        return res.status(201).json(films);

    } catch (error) {
        return res.status(500).json({ error: "Não foi possível listar todos os filmes!" });
    }
}

async function getFilteredFilms(req, res) {
    try {
        const { Op } = require("sequelize");
        const { genre } = req.body;
        const { classification } = req.body;
        

        const foundFilms = await Film.findAll({
            where: {
                [Op.or]: {
                    genre: genre,
                    classification: classification,
                },  
            }
        });

        console.log("chegou")
        
        if(!foundFilms) {
            return res.status(404).json({ message: "Não foi possível encontrar filmes com esses filtros." })
        }
        
        return res.status(200).send(foundFilms);
        
    } catch (error) {
        return res.status(500).json({ error: "Erro ao buscar filmes" })
    }
}

module.exports = {
    createFilm,
    getAllFilms,
    getFilteredFilms
}