const { Session, Seat } = require('../models/models');


async function createSession(req, res) {
    try {
        const { time, city, neighborhood, type } = req.body;

        const session = await Session.create({
            time,
            city,
            neighborhood,
            type,
        });

        for (let i= 0; i < 10 ; i++) {
            for (let j = 1; j < 19; j++) {
                const seat = await Seat.create({
                    seatNumber: j,
                    row: String.fromCharCode(i+65),
                    SessionId: session.id
                })
            }
        };

        return res.status(201).json(session);

    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao tentar registrar a sessão." });
        }
}

async function getAllSesions(req, res) {
    try {
        const sessions = await Session.findAll();

        if(!(sessions)) {
            return res.status(400).json({ error: "Não há sessões!" });
        }
            
        return res.status(201).json(sessions);

    } catch (error) {
        return res.status(500).json({ error: "Erro ao listar as sessões!" });
    }
}

async function getFilteredSessions(req, res) {
    try {
        const { Op } = require("sequelize");
        let { city } = req.body;
        let { neighborhood } = req.body;  
        let { type } = req.body    
        
        if(!neighborhood) {
            neighborhood = ""
        }
        if(!city) {
            city = ""
        }
        if(!type) {
            type = ""
        }

        console.log(neighborhood, type);

        const foundSessions = await Session.findAll({
            where: {
                [Op.or]: {
                    [Op.and]: {
                        type: type,
                        city: city,
                        neighborhood: neighborhood
                    },
                    [Op.and]: {
                        type: type,
                        city: city
                    },
                    [Op.and]: {
                        type: type,
                        neighborhood: neighborhood
                    },
                    [Op.and]: {
                        city: city,
                        neighborhood: neighborhood
                    },
                    [Op.or]: {
                        city: city,
                        neighborhood: neighborhood
                    }
                },  
            }
        });
        
        return res.status(200).json(foundSessions);
        
    } catch (error) {
        return res.status(500).json({ error: "Erro ao buscar sessões!" })
    }
}

module.exports = { 
    createSession,
    getAllSesions,
    getFilteredSessions
};