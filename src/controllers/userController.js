const User  = require('../models/user.models');


async function createUser(request, response){
    try {
        
        const{ name, lastname, cpf, birthDate, username, email, password } = request.body;

        const user = await User.create({
            name, 
            lastname, 
            cpf, 
            birthDate,
            username,
            email, 
            password
        });

        return response.status(201).json(user);
        
    } catch (error){
        return response.status(400)
        .json({ error: "Não foi possível criar o usuário" });
    }
}

async function validateLogin(req, res){
    try {
        const { Op } = require("sequelize")
        
        const { login } = req.body;
        const { password } = req.body;

        const foundUser = await User.findOne({
            where: {
                username: login,
            },
        });

        if(!foundUser) {
            return res.status(400).json({error: "Username not registered!"});
        }

        const foundPassword = await User.findOne({
            where: {
                [Op.and]: {
                    username: login,
                    password: password,
                }
            },
        });

        if(!foundPassword) {
            return res.status(400).json({error: "Incorrect password!"});
        }

        return res.status(200).json({ message: "Login efetuado"} )

    } catch(error){
        return res.status(500).json({ error: "Erro ao efetuar login."})

    }
}

module.exports = {
    createUser,
    validateLogin
}