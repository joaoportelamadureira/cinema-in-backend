const  User = require('../models/user.models');

async function checkUsernameExists(req, res, next){
    try {
        const { username } = req.body;

        const user = await User.findOne({
            where: {
                username,
            },
        });
        
        if(user){
            return res.status(400).json({error: "Esse usuário já existe!"});
        }

        return next();

    } catch(error){
        return res.status(500).json({ error: "Erro ao checar se o username já existe!"})

    }

    
    
}

module.exports = checkUsernameExists;