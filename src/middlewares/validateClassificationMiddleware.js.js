const User = require("../models/film.models");

async function validateClassification(req, res, next){
    try {
        const { classification } = req.body;

        if(classification < 0 || classification > 5) {
            return res.status(400).json( {error: "A classificação deve estar entre 0 e 5 inclusive."} )
        }

        return next();
        
    } catch (error) {
        return res.status(500).json({ error: "Erro ao tentar validar a classificação!"})
    }
}
module.exports = validateClassification;