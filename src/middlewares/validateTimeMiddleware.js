async function validateTime(req, res, next){
    try {
        const { time } = req.body;
        const regex = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;

        if(!(regex.test(time))) {
            return res.status(400).json( { error: "O horários devem respeitar o formato '00:00' "} );
        }

        return next()

                
    } catch (error) {
        return res.status(500).json({ error: "Erro ao tentar validar o horário da sessão!"})
    }
}

module.exports = validateTime;