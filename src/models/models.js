const { DataTypes } = require("sequelize");
const { v4:uuidv4 } = require("uuid");

const sequelize = require("../../config/database");

const Session = sequelize.define("Session", {
    id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
    },

    time: {
        type: DataTypes.STRING,
        allowNull: false
    },

    city: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ""
    },

    neighborhood: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ""
    },

    type: { //inteiro de 0 a 2 
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: ""
    },
});

const Seat = sequelize.define("Seat", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: () => uuidv4()
    },

    seatNumber: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    row: {
        type: DataTypes.STRING,
        allowNull: false
    },

    price: {
        type: DataTypes.FLOAT,
        allowNull: true
    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: true
    }
})

Session.hasMany(Seat, {foreignKey: "SessionId"} );

Seat.belongsTo(Session);


module.exports = {
    Session,
    Seat
}