const { DataTypes, UUID } = require("sequelize");
const { v4:uuidv4 } = require("uuid");

const sequelize = require("../../config/database");

const User = sequelize.define("User", {
    id: {
        type: UUID,
        defaultValue: () => uuidv4(),
        primaryKey:true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    lastname: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull: false,
        // verificação do número de dígitos? 00000000000
    },
    birthDate: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

module.exports = User;
