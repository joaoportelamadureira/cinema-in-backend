const express = require("express");
const filmeRoutes = express.Router();

//Inclusão dos Controllers
const filmController = require('../controllers/filmController');

//Inclusão dos Middlewares
const validateClassification = require('../middlewares/validateClassificationMiddleware.js');

// Criar filme
filmeRoutes.post('/', validateClassification, (req, res) => filmController.createFilm(req, res));

// Buscando filmes
filmeRoutes.get("/", (req, res) => filmController.getAllFilms(req, res));

//Filtrando filmes
filmeRoutes.get("/filtrado", (req, res) => filmController.getFilteredFilms(req,res));

module.exports = filmeRoutes;