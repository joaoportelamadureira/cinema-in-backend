const Router = require("express");
const router = Router();


// Rota filmes
const filmRoutes = require("./film.routes")
router.use('/filmes', filmRoutes);

// Rota usuários
const userRoutes = require("./user.routes");
router.use('/usuarios', userRoutes)

// Rotas sessões
const sessionRoutes = require("./session.routes");
router.use('/sessoes', sessionRoutes);

module.exports = router;