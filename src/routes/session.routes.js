const express = require("express");
const sessionRoutes = express.Router();

//Inclusão dos Controllers
const sessionController = require("../controllers/sessionController");

//Inclusão dos Middlewares
const validateTime = require("..//middlewares/validateTimeMiddleware")

//Criar Sessão
sessionRoutes.post('/', validateTime, (req, res) => sessionController.createSession(req, res));

// Buscar Sessões (todas)
sessionRoutes.get('/', (req, res) => sessionController.getAllSesions(req, res));

// Filtrando Sessões (por cidade e bairro)
sessionRoutes.get('/filtrado', (req, res) => sessionController.getFilteredSessions(req, res));

module.exports = sessionRoutes;