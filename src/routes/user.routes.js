const express = require("express");
const userRoutes = express.Router();

//Inclusão dos Controllers
const userController = require('../controllers/userController');

//Inclusão dos Middleware
const checkUsernameExists = require('../middlewares/checkUsernameExistsMiddleware');

// Criar usuário
userRoutes.post('/', checkUsernameExists, (req, res) => userController.createUser(req, res));

//validação Login
userRoutes.post("/login", (req, res) => userController.validateLogin(req, res));


module.exports = userRoutes;