const express = require('express');
const app = express();
const cors = require("cors");

app.use(cors());

const router = require('./routes/index');

const createDatabase = require("../scripts/createDatabase")

// Middleware para processar corpo JSON
app.use(express.json());

// Iniciando o servidor
const PORT = 3000;

// Servindo arquivos estáticos
app.use('/static', express.static(__dirname + '/public'));

//Roteamento vinculado ao nosso router
app.use(router);



app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});

try {
  createDatabase();
} catch (error) {
  console.log(error);
}